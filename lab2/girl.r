extractFeatures <- function(X, featureSet){
  i=5
  for (featurePresent in featureSet){
    if (!(featurePresent==1)){
      X = X[,-i]
    }
    i = i - 1
  }
  return(X)
}

linearRegression <- function(X, Y, nFolds){
  # Choose the best feature subset by trying everything!
  nFeats = dim(X)[2]
  resultsMat <<- matrix(ncol=nFeats+1)
  i = 1
  while (i<(2**nFeats)){
    #Define wich feature sub set we're going to use
    # featureSet is a binary number (in the form of vector) that defines 
    # which features should be present. 0 absent, 1 present
    featureSet = rev(as.integer(intToBits(i))[1:nFeats])
    DesignMatrix <- cbind(ones=1, extractFeatures(X, featureSet))
    ResponseVector <- Y
    # Divide data in nFolds
    # Are you sure this is the n you want to feed to errorStandardDeviation?
    n = dim(DesignMatrix)[1]
    p = dim(DesignMatrix)[2]
    testDataSize <<- floor(n/nFolds)
    sumOfErrors = 0
    for (k in 1:nFolds) {
      testDesignMatrix <<- DesignMatrix[((k-1)*testDataSize+1): (k*testDataSize),]
      trainDesignMatrix <<- DesignMatrix[-((k-1)*testDataSize+1): -(k*testDataSize),]
      trainResponseVector <<- ResponseVector[-((k-1)*testDataSize+1): -(k*testDataSize)]
      testResponseVector <<- ResponseVector[((k-1)*testDataSize+1): (k*testDataSize)]
      # Calculate
      # Size of slopeVector equals dim(DesignMatrix)[2]+1
      # print(dim(DesignMatrix)[2]+1)
      temp = t(trainDesignMatrix) %*% as.matrix(trainDesignMatrix)
      slopeVector <<- solve(temp) %*% t(trainDesignMatrix) %*% trainResponseVector
      # print(as.vector(slopeVector))
      fittedValuesVector <<- as.matrix(testDesignMatrix) %*% slopeVector
      residualsVector <<- testResponseVector - fittedValuesVector
      
      ErrorSquared = sum(residualsVector)^2
      sumOfErrors = sumOfErrors + ErrorSquared
    }
    
    meanError = sumOfErrors/n
    print(paste0("Feature subset is: ", paste(featureSet, collapse = "")))
    print(paste0("mean error for this feautures subset is: ", meanError))
    
    resultsMat <<- rbind(resultsMat, c(featureSet, meanError))
    
    i = i + 1
  }
  resultsMat <<- resultsMat[-1,]
  
  # Go through resultsMat, find lowest error, and return it!
  lowestI = 0
  bestError = 10000
  for(i in 1:(dim(resultsMat)[1])){
    if (resultsMat[i,nFeats+1] < bestError){
      lowestI = i
      bestError = resultsMat[i, nFeats+1]
    }
  }
  xAxis = apply(resultsMat, 1, function(x) paste(x[0:nFeats],collapse=""))
  yAxis = resultsMat[,nFeats+1]
  
  # Plot score of each feature subset
  png(file = "score_comparison.png")
  plot(c(1:((2**nFeats)-1)), yAxis, axes=FALSE, xlab = "subset used for testing", ylab = "Score of trained model", 
       main = "Subset score comparison")
  axis(2)
  axis(1, at=seq_along(c(1:((2**nFeats)-1))), labels=xAxis, las=2)
  dev.off()
  
  # Plot score of MSE vs number of features
  png(file = "mse_vs_nFeats.png")
  plot(1, resultsMat[1,(nFeats+1)], xlab = "Number of features", ylab = "Mean Squared Error", 
       main = "Subset score comparison", xlim=c(0, nFeats), ylim=c(20, 200))
  for(i in 2:nrow(resultsMat)){
    featureCount = sum(resultsMat[i, 1:nFeats])
    points(featureCount, resultsMat[i, nFeats+1])
  }
  dev.off()
  
  positions = which(resultsMat[lowestI,1:nFeats] %in% c(0))
  # print(positions)
  return(colnames(X[,-positions]))
}

set.seed(12345)
sampledSwiss = swiss[sample(nrow(swiss)),]
Y <- sampledSwiss$Fertility
X <- sampledSwiss[,-1]

result = linearRegression(X, Y, 5)
print(paste0("The optimal feature subset is ", paste(result, collapse = " ")))