#1 Import and plot
setwd('/Users/Valdieme/masters/MachineLearning/lab2')
tecatorData=read.csv2("tecator.csv")

png(file = "data_plot.png")
plot(tecatorData$Moisture, tecatorData$Protein, xlab="Moisture", ylab="Protein", main = "Moisture vs Protein")
dev.off()

#2
# The probablistic model that Mi describes is Polynomial multiple regression

#3 
halfOfRows = floor(nrow(moistureData)/2)
trainData = tecatorData[1:halfOfRows,]
testData = tecatorData[(halfOfRows+1):nrow(tecatorData),]

is <- c(1:6)
trainMSE <- c()
testMSE <- c()

fit1 = lm(Moisture ~ Protein, data=trainData )
trainMSE <- c(trainMSE, mean(fit1$residuals^2))
predic <- predict(fit1, testData)
testMSE <- c(testMSE, mean((predic-testData$Moisture)^2))

fit2 = lm(trainData$Moisture ~ trainData$Protein + I(trainData$Protein^2))
trainMSE <- c(trainMSE, mean(fit2$residuals^2))
predic <- predict(fit2, testData)
testMSE <- c(testMSE, mean((predic-testData$Moisture)^2))

fit3 = lm(trainData$Moisture ~ trainData$Protein + I(trainData$Protein^2) + I(trainData$Protein^3))
trainMSE <- c(trainMSE, mean(fit3$residuals^2))
predic <- predict(fit3, testData)
testMSE <- c(testMSE, mean((predic-testData$Moisture)^2))

fit4 = lm(trainData$Moisture ~ trainData$Protein + I(trainData$Protein^2) + I(trainData$Protein^3) + I(trainData$Protein^4))
trainMSE <- c(trainMSE, mean(fit4$residuals^2))
predic <- predict(fit4, testData)
testMSE <- c(testMSE, mean((predic-testData$Moisture)^2))

fit5 = lm(trainData$Moisture ~ trainData$Protein + I(trainData$Protein^2) + I(trainData$Protein^3) + I(trainData$Protein^4) + I(trainData$Protein^5))
trainMSE <- c(trainMSE, mean(fit5$residuals^2))
predic <- predict(fit5, testData)
testMSE <- c(testMSE, mean((predic-testData$Moisture)^2))

fit6 = lm(trainData$Moisture ~ trainData$Protein + I(trainData$Protein^2) + I(trainData$Protein^3) + I(trainData$Protein^4) + I(trainData$Protein^5) + I(trainData$Protein^6))
trainMSE <- c(trainMSE, mean(fit6$residuals^2))
predic <- predict(fit6, testData)
testMSE <- c(testMSE, mean((predic-testData$Moisture)^2))

# Plot MSEs of different Mi for train and test data
png(file = "mse_of_mi.png")
plot(is, trainMSE, xlab = "i", ylab = "Mean Squared Error of Mi", 
     main = "", xlim=c(1, 6), ylim=c(20, 200) , col="blue")

points(is, testMSE, col="red")
dev.off()

#4
library(MASS)
fit = lm(Fat ~ ., data=tecatorData[2:102])
# step <- stepAIC(fit, direction="both")
# summary(step)
# Running this line will output 
# Step:  AIC=95.55
# Fat ~ Channel1 + Channel2 + Channel4 + Channel5 + Channel7 + 
#   Channel8 + Channel11 + Channel12 + Channel13 + Channel14 + 
#   Channel15 + Channel17 + Channel19 + Channel20 + Channel22 + 
#   Channel24 + Channel25 + Channel26 + Channel28 + Channel29 + 
#   Channel30 + Channel32 + Channel34 + Channel36 + Channel37 + 
#   Channel39 + Channel40 + Channel41 + Channel42 + Channel45 + 
#   Channel46 + Channel47 + Channel48 + Channel50 + Channel51 + 
#   Channel52 + Channel54 + Channel55 + Channel56 + Channel59 + 
#   Channel60 + Channel61 + Channel63 + Channel64 + Channel65 + 
#   Channel67 + Channel68 + Channel69 + Channel71 + Channel73 + 
#   Channel74 + Channel78 + Channel79 + Channel80 + Channel81 + 
#   Channel84 + Channel85 + Channel87 + Channel88 + Channel92 + 
#   Channel94 + Channel98 + Channel99
# These 63 features were selected

# Exercise 5
covariates=scale(tecatorData[,2:101])
response=scale(tecatorData[, 102])
model0=glmnet(as.matrix(covariates),
              response, alpha=0,family="gaussian")
png(file = "ridge.png")
plot(model0, xvar="lambda", label=TRUE)
dev.off()

# Exercise 6
model0=glmnet(as.matrix(covariates),
              response, alpha=1,family="gaussian")
png(file = "lasso.png")
plot(model0, xvar="lambda", label=TRUE)
dev.off()

#Exercise 7
# Ommiting kfolds argument so the leave-one-out cross validation is performed
model0=cv.glmnet(as.matrix(covariates),
              response, alpha=1,family="gaussian")
png(file = "crossValidatedlasso.png")
plot(model0, xvar="lambda", label=TRUE)
dev.off()