library(geosphere)

# Get distance using geosphere library
distance<-function(p1, p2) {
  
  distm(p1, p2, fun = distHaversine)
}

# Provide it an array of distances!
gaussian<-function(data, width = 1) {
  return(exp(-((data/width)^2)))
}

kernDist <- function(sample, data)
{
  return(gaussian(distance(sample, data), widthDist))
}

kernDays <- function(sample, data)
{
  return(gaussian(sapply(data, dayDist, sample), widthDay))
}

kernHours <- function(sample, data)
{
  return(gaussian(sapply(data, hourDist, sample), widthHour))
}

dayDist <- function(d1, d2)
{
  return(circDist(d1, d2, 365))
}

hourDist <- function(h1, h2)
{
  return(circDist(h1 + 1, h2 + 1, 24))
}

circDist <- function(a, b, range)
{
  c <- max(a, b)
  d <- min(a, b)
  return(min( abs(c - d), abs(c - (d + range) ) ) )
}

toPosixDate<-function(date, time)
{
  return(as.POSIXlt(paste(date, time), tz = "GMT"))
}

predictDay <- function(dataFull, position, date)
{
  
  # Set up the points within the day
  predictionTimes <<- c( "02:00:00", "04:00:00", "06:00:00", "08:00:00", "10:00:00", "12:00:00",
                         "14:00:00", "16:00:00", "18:00:00", "20:00:00", "22:00:00", "24:00:00" )
  dois <<- toPosixDate(date, predictionTimes)
  
  numPoints = length(predictionTimes)
  
  day <<- dois[1]$yday
  res <<- numeric(length = numPoints)
  
  for( i in 1:numPoints )
  {
    doi = dois[i]
    data = dataFull[dataFull$fulldate < doi, ]
    # Locations
    locs = data[c("longitude", "latitude")]
    locWeights <<- kernDist(poi, locs)
    # dates
    dates <<- toPosixDate(data$date, data$time)
    dayWeights <<- kernDays(day, dates$yday)
    hourWeights <<- kernHours(doi$hour, dates$hour)
    
    weightSums = locWeights + dayWeights + hourWeights
    weightedTemps = weightSums * data$air_temperature
    
    sumWTemp = sum(weightedTemps)
    sumWeights = sum(weightSums)
    
    prediction = sumWTemp / sumWeights
    
    res[i] = prediction
  }
  res <<- res
  x = c(1:12)*2
  plot(x, res, xaxt = "n", xlab=paste("Time of day ", date), ylab="Temperature (C)" )
  axis(1, at=x, labels=predictionTimes)
}

predictSingle <- function(dataFull, position, doi)
{
  data = dataFull[dataFull$fulldate < doi, ]
  # Locations
  locs = data[c("longitude", "latitude")]
  locWeights = kernDist(position, locs)
  # dates
  dates = toPosixDate(data$date, data$time)
  dayWeights = kernDays(doi$yday, dates$yday)
  hourWeights = kernHours(doi$hour, dates$hour)

  weightSums = locWeights + dayWeights + hourWeights
  weightedTemps = weightSums * data$air_temperature

  sumWTemp = sum(weightedTemps)
  sumWeights = sum(weightSums)

  prediction = sumWTemp / sumWeights

  res = prediction
  # x = c(1:12)*2
  # plot(x, res, xaxt = "n", xlab=paste("Time of day ", date), ylab="Temperature (C)" )
  # axis(1, at=x, labels=predictionTimes)
}


setwd('/Users/Valdieme/masters/MachineLearning/lab5')

stations=read.csv("stations.csv")
temps=read.csv("temps50k.csv")

set.seed(12345)
st <- merge(stations,temps,by="station_number")

st$fulldate <- toPosixDate(st$date, st$time)

n=dim(st)[1]
id=sample(1:n, floor(n*0.99))
train = st[id,]
valid = st[-id,]
valPoses = valid[c("longitude", "latitude")]
valDates = valid$fulldate
valTemps = valid$air_temperature


# # Point and Time of Interest
# positionLatitude   <- 58.4274
# positionLongitude  <- 14.826
# predictionDate <- "2013-11-04"
# 
# poi <- c(positionLongitude, positionLatitude)

# nTest = dim(valid)[1]
nTest = min(500, dim(valid)[1])

distOpts = c(60000)
daysOpts = c(20, 30, 35, 40, 45, 50, 60)
hourOpts = c(1)

plottable = numeric(length = (length(distOpts) * length(daysOpts) * length(hourOpts)))
ind = 0;


for( dis in distOpts ) {
  for ( day in daysOpts ) {
    for ( hour in hourOpts ) {
      
      ind = ind + 1
      widthDist <<- dis
      widthDay  <<- day
      widthHour <<- hour
      
      res = numeric(length = nTest)
      
      nSamps = c(1:nTest)
      # nSamps = c(1:25)
      
      for( i in nSamps ) {
      # for( i in 1:25) {
        
        res[i] = predictSingle(train, valPoses[i,], valDates[i])
      #  print(paste(i, res[i], valTemps[i]))
      }
      
      err = res - valTemps[1:nTest]
      
      # hist(abs(err), col = "red", ylab = "# Instances", xlab = "Abs. Error", main = "Histogram of absolute errors")
      # hist(err, col = "red", ylab = "# Instances", xlab = "Abs. Error", main = "Histogram of errors")
      
      mse = mean(err^2)
      
      print( paste( widthDist, widthDay, widthHour ) )
      print( mse )
      plottable[ind] = mse
    }
  }
}

plottable <<- plottable



