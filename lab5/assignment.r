#Set up
library(plyr)
library(geosphere)
setwd('/Users/Valdieme/masters/MachineLearning/lab5')
set.seed(12345)

stations=read.csv(file="stations.csv")
temps=read.csv("temps50k.csv")

# Merge two data frames by common columns
st <- merge(stations,temps,by="station_number")
st <- st[sample(nrow(st)),]
st <- st[1:5000,]

# The point to predict (up to the students)
POIlat <- 58.4274
POIlon <- 14.826
# The date to predict (up to the students)
DOI <- "2013-11-04"
times <- c("04:00:00", "06:00:00", "08:00:00", "10:00:00", "12:00:00", 
           "14:00:00", "16:00:00", "18:00:00", "20:00:00", "22:00:00", 
           "24:00:00")
TOI <- times[1] 

x = c(POIlat, POIlon, DOI, TOI)
names(x) = c("latitude", "longitude", "date", "time")
x = data.frame(lapply(x, type.convert), stringsAsFactors=FALSE)

distBetweenPoints <- function(datRow1, datRow2){
  return(distm(c(datRow1$longitude, datRow1$latitude), c(datRow2$longitude, datRow2$latitude), fun = distHaversine))
}

distBetweenDates <- function(datRow1, datRow2) {
  res = as.numeric(as.Date(as.character(datRow1$date), format="%Y-%m-%d") - as.Date(as.character(datRow2$date), format="%Y-%m-%d"))
  absRes = abs(res) %% 365
  if (absRes > 183) {
    absRes = 365-absRes
  }
  return(absRes)
}

distBetweenTimes <- function(datRow1, datRow2) {
  temp = as.numeric(strptime(datRow1$time, "%H:%M:%OS") - strptime(datRow2$time, "%H:%M:%OS"))
  absTemp = abs(temp) %% 24
  if (absTemp > 12){
    absTemp = 24 - absTemp
  }
  return(absTemp)
}

# There should be three Gaussian kernel methods
g <- function(x, xPrime, width, distFun){
  dX = distFun(x, xPrime) / width
  return(exp(-(dX^2)))
}
# The first to account for the distance from a station to the point of interest.
g1 <- function(x, xPrime, width){
  return(g(x, xPrime, width, distBetweenPoints))
}
# The second to account for the distance between the day a temperature measurement was made and the day of interest.
g2 <- function(x, xPrime, width) {
  return(g(x, xPrime, width, distBetweenDates))
}
# The third to account for the distance between the hour of the day a temperature mea- surement was made and the hour of interest.
g3 <- function(x, xPrime, width) {
  return(g(x, xPrime, width, distBetweenTimes))
}

# We now build our final kernel function as a weighted sum of the previous
finalKernelMethod <- function(x, xPrime, weights) {
  res1 = g(x, xPrime, weights[1], distBetweenPoints)
  res2 = g(x, xPrime, weights[2], distBetweenDates)
  res3 = g(x, xPrime, weights[3], distBetweenTimes)
  #print(res1)
  #print(res2)
  #print(res3)
  return(res1)
  # return(res1 + res2 + res3)
}

gPredict <- function(newData, weights){
  res <<- adply(st, 1, finalKernelMethod, newData, weights)
  # res = res[,ncol(res)]
  # lol = c(1:(dim(st)[1]))
  # for (i in 1:(dim(st)[1])){
  #   lol[i] = finalKernelMethod(st[i,], newData, weights)
  # }
  # print(res[1:50])
  # print(lol[1:50])
  # print(sum((res * st$air_temperature)))
  final = (sum(res[,ncol(res)] * res$air_temperature)) / sum(res[,ncol(res)])
  return(final)
}


predict <- function(){
  pred=c(1:length(times))
  for (i in 1:length(times)) {
    x$time = times[i]
    pred[i] = gPredict(x)
  }
  
  plot(c(1:length(times)), pred, type="o", xaxt="n", xlab="Times")
  axis(1, at=1:length(times), labels=times)
}



kfoldCV <- function(data, k){
  dataChunksSize = floor(nrow(data)/k)
  for (ki in 1:k) {
    testData = data[((ki-1)*dataChunksSize+1): (ki*dataChunksSize),]
    trainData = data[-((ki-1)*dataChunksSize+1): -(ki*dataChunksSize),]
    
    
  }
}