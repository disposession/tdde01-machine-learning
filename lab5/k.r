#Set up
library(geosphere)
setwd('/Users/Valdieme/masters/MachineLearning/lab5')
set.seed(12345)

stations=read.csv(file="stations.csv")
temps=read.csv("temps50k.csv")

# Merge two data frames by common columns
st <- merge(stations,temps,by="station_number")
st$date <- sapply(st$date, as.Date)
st <- st[sample(nrow(st)),]
# st <- st[1:25000,]

# The point to predict (up to the students)
POIlat <- 58.4274
POIlon <- 14.826
# The date to predict (up to the students)
DOI <- "2013-08-15"
times <- c("04:00:00", "06:00:00", "08:00:00", "10:00:00", "12:00:00", 
           "14:00:00", "16:00:00", "18:00:00", "20:00:00", "22:00:00", 
           "24:00:00")
TOI <- times[1] 

x = c(POIlat, POIlon, DOI, TOI)
names(x) = c("latitude", "longitude", "date", "time")
x = data.frame(lapply(x, type.convert), stringsAsFactors=FALSE)
x$date <- sapply(x$date, as.Date)

distBetweenPoints <- function(datRow1, datRow2){
  xPoint = datRow1[,c("longitude", "latitude")]
  yPoint = datRow2[,c("longitude", "latitude")]
  return(distm(xPoint, yPoint, fun = distHaversine))
}

distBetweenDates <- function(datRow1, datRow2) {
  res = datRow1$date - datRow2$date
  res = sapply(res, function(x){
    absX = abs(x) %% 365
    if (absX > 183) {
      absX = 365-absX
    }
    return(absX)
  })
  return(res)
}

distBetweenTimes <- function(datRow1, datRow2) {
  # res = as.numeric(strptime(datRow1$time, "%H:%M:%OS") - strptime(datRow2$time, "%H:%M:%OS"))
  res = apply(datRow1, 1, function(x){
    x = as.numeric(strptime(x["time"], "%H:%M:%OS") - strptime(datRow2$time, "%H:%M:%OS"))
    absX = abs(x) %% 24
    if (absX > 12){
      absX = 24 - absX
    }
    return(absX)
  })
  return(res)
}

# The three Gaussian kernel methods are defined by what distance function is used here.
g <- function(x, xPrime, width, distFun){
  dX = distFun(x, xPrime) / width
  return(exp(-(dX^2)))
}

# The final kernel function as a weighted sum of the previous
finalKernelMethod <- function(x, xPrime, weights) {
  res1 = g(x, xPrime, weights[1], distBetweenPoints)
  res2 = g(x, xPrime, weights[2], distBetweenDates)
  res3 = g(x, xPrime, weights[3], distBetweenTimes)
  return((res1 + res2 + res3)/3)
}

gPredict <- function(trainData, newData, weights){
  # Can't know the future! Remove data points that are after the prediction time
  trainData = trainData[trainData$date < newData$date, ]
  res <- finalKernelMethod(trainData, newData, weights)
  # plot(res, type="p")
  final = (sum(res * trainData$air_temperature)) / sum(res)
  return(final)
}

kfoldCV <- function(data, k, weights){
  dataChunksSize = floor(nrow(data)/k)
  residualSums = 0
  for (ki in 1:k) {
    testData <<- data[((ki-1)*dataChunksSize+1): (ki*dataChunksSize),]
    trainData <<- data[-((ki-1)*dataChunksSize+1): -(ki*dataChunksSize),]
    
    foldResidual = 0
    
    for(i in 1:(dim(testData)[1])){
      #print(i)
      pred = gPredict(trainData, testData[i,], weights)
      actual = testData[i,]$air_temperature
      foldResidual = foldResidual + (pred-actual)^2
    }
    residualSums = residualSums + foldResidual
  }
  return(residualSums/k)
}

anotherCV <- function(data, weights){
  n = dim(data)[1]
  #Test data is 1% of data
  testData <- data[(0.99*n+1):n,]
  # Train data is 99% of data
  trainData <- data[1:(0.99*n),]
  
  residualSum = 0
  
  for(i in 1:(dim(testData)[1])){
    #print(i)
    pred = gPredict(trainData, testData[i,], weights)
    actual = testData[i,]$air_temperature
    residualSum = residualSum + (pred-actual)^2
  }
  residualSum = residualSum / dim(testData)[1]
  return(residualSum)
}
nestedCV_w1 <- function(){
  # Finding Optimal w1
  best = 4000
  bestI = 0
  J = 45
  K = 8
  iValues <- seq(30000, 100000, by = 10000)
  iRes <- iValues
  for(i in 1:length(iValues)){
    print("here we go")
    result = anotherCV(st, c(iValues[i],J,K))
    iRes[i] = result
    print(result)
  }
  png("MSE_for_w1.png")
  plot(iValues, iRes, type="o", xlab="distance weight", main="MSE as function of physical distance weight", ylab="MSE")
  dev.off()
}

nestedCV_w2 <- function(){
  # Finding Optimal w2
  best = 4000
  I = 150000
  bestJ = 0
  K = 1
  jValues <- seq(10, 180, by = 10)
  jRes <- jValues
  for(i in 1:length(jValues)){
    print("here we go")
    result = anotherCV(st, c(I,jValues[i],K))
    jRes[i] = result
    print(result)
  }
  png("MSE_for_w2.png")
  plot(jValues, jRes, type="o", xlab="distance weight", main="MSE as function of day distance weight", ylab="MSE")
  dev.off()
}
# Finding Optimal w3
nestedCV_w3 <- function(){
  best = 4000
  I = 150000
  J = 45
  bestK = 1
  kValues <- seq(0.5, 12, by = 0.5)
  kRes <- kValues
  for(i in 1:length(kValues)){
    print("here we go")
    result = anotherCV(st, c(I,J,kValues[i]))
    kRes[i] = result
    print(result)
  }
  png("MSE_for_w3.png")
  plot(kValues, kRes, type="o", xlab="distance weight", main="MSE as function of time distance weight", ylab="MSE")
  dev.off()
}

nestedCV_comb <- function(){
  # Finding optimal combo
  best = 4000
  bestI = 0
  bestJ = 0
  bestK = 0
  for(i in seq(50000, 200000, by = 10000)){
    for(j in seq(20, 70, by = 10)){
      for(k in seq(0.5, 7, by = 0.5)){
        result = anotherCV(st, c(i,j,k))
        print(result)
        if (result < best){
          print("Found new best!")
          print(i)
          print(j)
          print(k)
          best = result
          bestI = i
          bestJ = j
          bestK = k
        }
      }
    }
  }
}


# Predict target day. Use weights 850k, 1 and 60 as
predict <- function(){
  pred=c(1:length(times))
  for (i in 1:length(times)) {
    print(times[i])
    x$time = times[i]
    pred[i] = gPredict(st, x, c(20000, 20, 7))
  }
  
  plot(c(1:length(times)), pred, type="o", xaxt="n", xlab="Times", main="Prediction for 2013-08-15 on p(58.427, 14.826)", ylab="Predicted values")
  axis(1, at=1:length(times), labels=times)
}