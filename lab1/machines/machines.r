# Source for the used likelihood/loglikelihood/maxlikelihood functions
# https://www.statlect.com/fundamentals-of-statistics/exponential-distribution-maximum-likelihood

# 1. Import the data to R.

setwd('/Users/Valdieme/masters/MachineLearning/lab1/machines')
machinesDF=read.csv2("machines.csv")

# What is the distribution type of x? 
# It's a exponential distribution

# Dataset machinesDF and model p(x|theta) = theta*e^(-theta*x)

# Compute log likelihood p(D|w) = n*ln(D) - D*Sum(X)
logLikelihood <- function(D, Wvector ) {
  t1 = dim(Wvector)[1] * log(D)
  t2 = D * sum(Wvector)
  return(t1-t2)
}

maxLikelihoodEstimator <- function(Wvector){
  return(dim(Wvector)[1] / sum(Wvector))
}
#2 
theta = seq(0.1, 4, 0.1)
llh = lapply(theta, logLikelihood, machinesDF)
# Plot LogLikelihood for different theta
png(file = "log_likelihood_distribution.png")
# Plot the bar chart.
plot(theta, llh, type = "o", xlab = "theta", ylab = "loglikelihood value", 
     main = "Log likelihood for theta", xlim=c(0,4), ylim=c(-100,-20))
dev.off()
print(paste0("Optimal theta for full dataset is: ", maxLikelihoodEstimator(machinesDF)))

#3
llh6 = lapply(theta, logLikelihood, head(machinesDF))
# Plot LogLikelihood for different theta
png(file = "log_likelihood_full_vs_partial_data.png")
# Plot the bar chart.
# When plotting, normalize the vectors so that they both fit the same "dimensional space"
plot(theta, lapply(llh, function(x, n){x/n}, length(llh)), type = "o", col = "red", xlab = "theta", ylab = "loglikelihood value", 
     main = "Normalized Log likelihood for theta on full dataset vs partial dataset", xlim=c(0,4), ylim=c(-4,0))
lines(theta, lapply(llh6, function(x, n){x/n}, length(llh6)), type = "o", col = "blue")
legend("bottomright", c("partial data","full data"), lty=c(1,1), lwd=c(2.5,2.5), col=c("blue","red"))
dev.off()
print(paste0("Optimal theta for partial dataset is: ", maxLikelihoodEstimator(head(machinesDF))))

# 4
# This is the likelihood function from
likelihood <- function(x, theta){
  n = dim(x)[1]
  return(theta^n * exp(-theta*sum(x)))
}

prior <- function(theta){
  return(10*exp(-10*theta))
}

l <- function(theta){
  log(likelihood(machinesDF, theta) * prior(theta));
}

ltheta = lapply(theta, l)
# Plot LogLikelihood for different theta
png(file = "likelihood_distribution.png")
# Plot the bar chart.
plot(theta, ltheta, type = "o", xlab = "theta", ylab = "loglikelihood value", 
     main = "Log likelihood taking prior to account", xlim=c(0,4), ylim=c(-100,-20))
dev.off()
optimalTheta = theta[which.max(ltheta)]
print(paste0("Optimal theta for l(theta) function is ", optimalTheta))

# 5
set.seed(12345)
newLengths = rexp(50, maxLikelihoodEstimator(machinesDF))
png(file = "histogram_original_data.png")
hist(t(as.vector(machinesDF)), main="Original data histogram", xlab = "Original data points")
dev.off()
png(file = "histogram_new_data.png")
hist(t(as.vector(newLengths)), main="New data histogram", xlab = "New data points")
dev.off()