library(kknn)
# Compute Distance Matrix between matrices X and Y
distanceMatrix <- function(X,Y){
  Xhat = X / sqrt(rowSums(X^2))
  Yhat = Y / sqrt(rowSums(Y^2))
  resMat = as.matrix(Xhat) %*% t(Yhat)
  return(1-resMat)
}

# Count how many of the k neighbors are spam
countSpam <- function(row, data){
  spam = 0;
  for(i in row){
    spam = spam + data[i, ncol(data)]
  }
  return(spam)
}

# Label documents as spam or not depending on their probability of being one and the threshold
labelDoc <- function(probValue, threshold) {
  if(probValue > threshold) {
    return(1)
  }
  return(0)
}

knearest <- function(data, K, newdata){
  # Data is the training data
  # K is the number of closest neighbors
  # newdata is the data to classify
  
  # Calculate distance matrix (need to strip label collumn)
  distanceM <- distanceMatrix(data[,-length(data)], newdata[,-length(newdata)])
  #For each row of newdata find k closest points
  kclosest <- t(apply(distanceM, 2, order)[ 1:K, ])
  if(K==1){
    kclosest = t(kclosest);
  }
  # Determine how many are labeled as spam 
  labeledSpam <- apply(kclosest, 1, countSpam, data)

  #Calculate probability
  probMatrix <- labeledSpam / K
}

labelFromProbMatrix <- function(probMatrix, threshold){
  # Label data approprietely
  finalClassification <- sapply(probMatrix, labelDoc, threshold)
  return(finalClassification)
}


tableAccuracy <- function(myTable){
  return( (myTable[1,1] + myTable[2,2]) / sum(sum(myTable)))
}

setwd('/Users/Valdieme/masters/MachineLearning/lab1/knearest');
Dataframe=read.csv2("spambase.csv");

n=dim(Dataframe)[1];
set.seed(12345);
id=sample(1:n, floor(n*0.5));
train=Dataframe[id,];
test=Dataframe[-id,];


############### Classify using k=5 #############
probMatrix = knearest(train, 5, test)
classified = labelFromProbMatrix(probMatrix, 0.5)
confusionMatrix = table(test$Spam, classified)
confusionMatrix
print(paste0("Missclassification rate  for k=5 in test data: ", tableAccuracy(confusionMatrix)))
print("Confusion Matrix is")
print(confusionMatrix)
probMatrix = knearest(train, 5, train)
classified = labelFromProbMatrix(probMatrix, 0.5)
confusionMatrix = table(train$Spam, classified)
confusionMatrix
print(paste0("Missclassification rate  for k=5 in train data: ", tableAccuracy(confusionMatrix)))
print("Confusion Matrix is")
print(confusionMatrix)

############### Classify using k=1 #############
probMatrix = knearest(train, 1, test)
classified = labelFromProbMatrix(probMatrix, 0.5)
confusionMatrix = table(test$Spam, classified)
confusionMatrix
print(paste0("Missclassification rate  for k=1 in test data: ", tableAccuracy(confusionMatrix)))
print("Confusion Matrix is")
print(confusionMatrix)
probMatrix = knearest(train, 1, train)
classified = labelFromProbMatrix(probMatrix, 0.5)
confusionMatrix = table(train$Spam, classified)
confusionMatrix
print(paste0("Missclassification rate  for k=1 in train data: ", tableAccuracy(confusionMatrix)))
print("Confusion Matrix is")
print(confusionMatrix)

############### Classify using knn package and k=5 #############
kasd=kknn(formula=Spam~., train=train, test=test,k=5)
knnResults = sapply(kasd$fitted.values, labelDoc, 0.5)

confusionMatrix = table(test$Spam, knnResults)
confusionMatrix
print(paste0("Missclassification rate for k=5 in knn: ", tableAccuracy(confusionMatrix)))
print("Confusion Matrix is")
print(confusionMatrix)

x <- 0
# The accuracy, specificity and sensitivity of  
kknnAccuracy <- c()
kknnSpecificity <- c()
kknnSensitivity <- c()

knearestAccuracy <- c()
knearestSpecificity <- c()
knearestSensitivity <- c()

# train kknn
kknnResult = kknn(formula=Spam~., train=train, test=test,k=5)
kknnProbMatrix = kknnResult$fitted.values
# train knearest
knearestProbMatrix = knearest(train, 5, test)

#Classify using different thresholds
while(x < 20) {
  # kknn
  kknnClassification = sapply(kknnProbMatrix, labelDoc, x/20)
  confusionMatrix = table(test$Spam, kknnClassification)
  # Calculate accuracy
  kknnAccuracy[x+1] = tableAccuracy(confusionMatrix)
  # Calculate Sensitivity (True positives)
  kknnSensitivity[x+1] = confusionMatrix[2,2] / (rowSums(confusionMatrix)[2])
  # Calculate False Positive Rates
  kknnSpecificity[x+1] =(confusionMatrix[1,2] / (rowSums(confusionMatrix)[1]))
  
  # knearest
  knearestClassification = labelFromProbMatrix(knearestProbMatrix, x/20)
  confusionMatrix = table(test$Spam, knearestClassification)

  # Calculate accuracy
  knearestAccuracy[x+1] = tableAccuracy(confusionMatrix)
  # Calculate Sensitivity (True positives)
  knearestSensitivity[x+1] = confusionMatrix[2,2] / (rowSums(confusionMatrix)[2])
  # print(knearestSensitivity[x+1])
  # Calculate FPR
  knearestSpecificity[x+1] = (confusionMatrix[1,2] / (rowSums(confusionMatrix)[1]))
  
  # Up 'it' goes
  x <- x+1
}

x<-c(0:19)/20
# Plot the sensitivity
png(file = "sensitivy_chart.png")

plot(x, knearestSensitivity*100,type = "o",col = "red", xlab = "rate", ylab = "Accuracy %", 
     main = "Sensitivity knn vs knearest", ylim=c(0, 100))
lines(x, kknnSensitivity*100, type = "o", col = "blue")
legend("topright",c("kknn","knearest"), lty=c(1,1), lwd=c(2.5,2.5), col=c("blue","red")) 
# Save the file.
dev.off()

# Plot the specificity
png(file = "specificity_chart.png")

plot(x, (1-knearestSpecificity)*100,type = "o",col = "red", xlab = "rate", ylab = "Accuracy %", 
     main = "Sensitivity knn vs knearest", ylim=c(0, 100))
lines(x, (1-kknnSpecificity)*100, type = "o", col = "blue")
legend("topright",c("kknn","knearest"), lty=c(1,1), lwd=c(2.5,2.5), col=c("blue","red")) 
# Save the file.
dev.off()

print("See knearest_ROC_plot.png for ROC curves")
# ROC plot (Use the fpr for x-axis, )
png(file = "knearest_ROC_plot.png")
# Plot the bar chart.
plot(kknnSpecificity, kknnSensitivity,type = "o",col = "red", xlab = "fpr", ylab = "tpr", 
     main = "ROC curve for knearest", xlim=c(0,1), ylim=c(0, 1))
lines(knearestSpecificity, knearestSensitivity, type = "o", col = "blue")

legend("topright", c("knearest","kknn"), lty=c(1,1), lwd=c(2.5,2.5), col=c("blue","red"))
# Save the file.
dev.off()