#Set up
library(neuralnet)
setwd('/Users/Valdieme/masters/MachineLearning/lab6')
set.seed(1234567890)

Var <- runif(50, 0, 10)
trva <- data.frame(Var, Sin=sin(Var))
tr <- trva[1:25,] # Training
va <- trva[26:50,] # Validation

# Random initializaiton of the weights in the interval [-1, 1]
numNodes = 10
winit <- runif(numNodes*3+1, -1, 1)

#Test with different thresholds
MSEs = c(1:10)
thresholds = c(1:10)
bestMSE = 100000
bestNN = NA
for(i in 1:10) {
  # train neural network
  nn <- neuralnet(Sin ~ Var, data=tr, hidden=c(numNodes), threshold=(i/1000), startweights = winit)
  # plot(nn, rep = "best")
  prediction <- compute(nn, va$Var)
  # calculate MSE for this threshold
  error = prediction$net.result - va$Sin
  MSEs[i] = (sum(error ^ 2) / length(error))
  thresholds[i] = i/1000
  if (MSEs[i] < bestMSE){
    bestMSE = MSEs[i]
    bestNN = nn
  }
}

plot(thresholds, MSEs, type="o")
plot(bestNN, rep="best")
#   # Plot of the predictions (black dots) and the data (red dots)
prediction <- compute(nn, trva$Var)
plot(trva$Var, prediction$net.result, xlab="Value", ylab="sine")
points(trva$Var, trva$Sin, col = "red")
legend("bottomright",c("original","predicted"), lty=c(1,1), lwd=c(2.5,2.5), col=c("black","red")) 

newVar = Var <- runif(150, -10, 20)
prediction <- compute(nn, newVar)
plot(newVar, prediction$net.result, xlab="Value", ylab="NN prediction")