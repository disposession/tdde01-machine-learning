#Set up
library(tree)
library(boot)
setwd('/Users/Valdieme/masters/MachineLearning/lab4')
set.seed(12345)

# Exercise 1
statesData=read.csv2("State.csv")
# Order by MET
statesData <- statesData[with(statesData, order(MET)), ]

# Plot MET as function of EX
png("nope.png")
plot(statesData$MET, statesData$EX)
dev.off()

# Exercise 2
# Fit tree with cross validation (min observations on terminal nodes should be 8)
fit = tree(EX ~ MET, data = statesData, control = tree.control(nobs=nrow(statesData), minsize=8))
fitcv = cv.tree(fit)
# Plot size of tree vs deviance
png("size_vs_deviance.png")
plot(fitcv$size, fitcv$dev, type="b", col="red", xlab="Tree Size", ylab="Tree Deviance")
dev.off()

# From plot, optimal tree is with size = 3
finalTree=prune.tree(fit, best=3)

# PLot original vs fitted data
png("original_vs_fitted.png")
plot(statesData$MET, statesData$EX, col="blue", ylim=c(0,500), xlab = "MET", ylab = "EX", main = "Original vs Fitted Data")
predictedValues = predict(finalTree, newdata=statesData)
points(statesData$MET, as.vector(predictedValues), col="red")
legend("bottomright",c("original","fitted"), lty=c(1,1), lwd=c(2.5,2.5), col=c("blue","red")) 
dev.off()

# Histogram of residuals
png("residuals_histogram.png")
hist(resid(finalTree), plot=TRUE)
dev.off()

#Summary of tree
print(summary(finalTree))

# Exercise 3 - Non parametric bootstrap

# computing bootstrap samples
# Linear regression predictor function
dataSample <- NA
f=function(data, ind){
  # extract bootstrap sample
  dataSample <<- data[ind,]
  # Fit new tree
  fit1 = tree(EX ~ MET, data = dataSample, control = tree.control(nobs=nrow(dataSample), minsize=8))
  # Should we cross validate
  #fitcv1 = cv.tree(fit1)
  #predict values using the optimal tree
  finalTree1 = prune.tree(fit1, best=3)
  predictedEX1 = predict(finalTree1, newdata = statesData)
  return(predictedEX1)
}
bootstrapRes = boot(statesData, f, R=1000) #make bootstrap
enveBoot = envelope(bootstrapRes, level=0.95)

print("Bootstrap result")
png("swiggity_swooty.png")
plot(statesData$MET, statesData$EX, col="blue", ylim=c(0,500))
points(statesData$MET, as.vector(predictedValues), col="red")
lines(statesData$MET, enveBoot$point[1,], col="green")
lines(statesData$MET, enveBoot$point[2,], col="green")
dev.off()

# Exercise 4 - Parametric bootstrap
# mle=finalTree
mle = tree(EX ~ MET, data = statesData, control = tree.control(nobs=nrow(statesData), minsize=8))

rng=function(data, mle) {
  data1= data[, c("MET", "EX")]
  n=length(data1$EX)
  data1$EX = rnorm(n,predict(mle, newdata=data1),sd(resid(mle)))
  return(data1)
}

f1=function(data1){
  # Fit new tree
  fit1 = tree(EX ~ MET, data = data1, control = tree.control(nobs=nrow(data1), minsize=8))
  finalTree1 = prune.tree(fit1, best=3)
  predictedEX1 = predict(finalTree1, newdata = statesData)
  return(predictedEX1)
}

parametricBootRes=boot(statesData, statistic=f1, R=1000, mle=mle,ran.gen=rng, sim="parametric")
enveParamBoot = envelope(parametricBootRes, level=0.95)

# Prediction bands
f2=function(data1){
  # Fit new tree
  fit1 = tree(EX ~ MET, data = data1, control = tree.control(nobs=nrow(data1), minsize=8))
  finalTree1 = prune.tree(fit1, best=3)
  predictedEX1 = predict(finalTree1, newdata = statesData)
  n = length(statesData$EX)
  predictedEX = rnorm(n, predictedEX1, sd(resid(mle)))
  return(predictedEX)
}

parametricPredBootRes=boot(statesData, statistic=f2, R=1000, mle=mle,ran.gen=rng, sim="parametric")
enveParamPredBoot = envelope(parametricPredBootRes, level=0.95)

print("Parametric Bootstrap result")
png("parametric_swiggity_swooty.png")
plot(statesData$MET, statesData$EX, col="blue", ylim=c(0,500), main="Parametric bootstrap", xlab = "MET", ylab = "EX")
points(statesData$MET, as.vector(predictedValues), col="red")
lines(statesData$MET, enveParamBoot$point[1,], col="green")
lines(statesData$MET, enveParamBoot$point[2,], col="green")
lines(statesData$MET, enveParamPredBoot$point[1,], col="purple")
lines(statesData$MET, enveParamPredBoot$point[2,], col="purple")
legend("bottomright",c("confidence bands","prediction bands"), lty=c(1,1), lwd=c(2.5,2.5), col=c("green","purple")) 
dev.off()



# Exercise 5
png("parametric_vs_unparametric_swiggity_swooty.png")
plot(statesData$MET, statesData$EX, col="blue", ylim=c(150,470), main="Parametric vs Unparametric bootstrap", xlab = "MET", ylab = "EX")
points(statesData$MET, as.vector(predictedValues), col="red")
lines(statesData$MET, enveParamBoot$point[1,], col="green")
lines(statesData$MET, enveParamBoot$point[2,], col="green")
lines(statesData$MET, enveBoot$point[1,], col="purple")
lines(statesData$MET, enveBoot$point[2,], col="purple")
legend("bottomright",c("parametric","non-parametric"), lty=c(1,1), lwd=c(2.5,2.5), col=c("green","purple")) 
dev.off()