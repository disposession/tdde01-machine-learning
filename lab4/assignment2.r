#Set up
library(fastICA)
library(pls)
setwd('/Users/Valdieme/masters/MachineLearning/lab4')
set.seed(12345)

# Exercise 1
# Import data
CollectedData=read.csv2("NIRSpectra.csv")
# Conduct a standard PCA by using the feature space
# data1$Fat=c()
res=prcomp(CollectedData[,-127])

#eigenvalues
lambda=res$sdev^2

# Plot with how much variation is explained by each feature.
print(sprintf("%2.3f",lambda/sum(lambda)*100))
png("weight_of_features.png")
screeplot(res)
dev.off()

# By using the two first features, we get approx 99.643 of the source of variance in the data.

# Provide also a plot of the scores in the coordinates (PC1, PC2). 
png("data_in_two_dim.png")
plot(res$x[,1], res$x[,2], xlab = "PC1", ylab = "PC2")
dev.off()
# Are there unusual diesel fuels according to this plot?
# When plotting the data using only these two dimensions we can see the data being quite cluttered which seems to show that indded these two dimensions suffice. Furthermore, the variation within PC2 is augmented by only two datapoints which indicates that these might be outliers and therefore making PC2 even less significant in variance.

# Exercise 2
# Traceplots 
U=res$rotation
png("traceplot_pc1.png")
plot(U[,1], main="Traceplot, PC1", xlab="Component#", ylab="Loading", ylim=c(0,0.4))
dev.off()
png("traceplot_pc2.png")
plot(U[,2], main="Traceplot, PC2", xlab="Component#", ylab="Loading", ylim=c(-0.1,0.4))
dev.off()

# 3 a)
resICA <- fastICA(CollectedData[, -127], 2, alg.typ = "parallel", fun = "logcosh", alpha = 1, method = "R", row.norm = FALSE, maxit = 200, tol = 0.0001, verbose = TRUE) #ICA
w <- resICA$K %*% resICA$W
png("traceplot_w1.png")
plot(w[,1], main="Traceplot, W' 1", ylim=c(-12, 2))
dev.off()
png("traceplot_w2.png")
plot(w[,2], main="Traceplot, W' 2", ylim=c(-12, 2))
dev.off()

# 3 b)
png("w'_data_in_two_dim.png")
plot(resICA$S[,1], resICA$S[,2], xlab = "W'1", ylab = "W'2")
dev.off()

# 4 PCR
pcr.fit=pcr(Viscosity ~ ., data=CollectedData, validation="CV")
summary(pcr.fit)
png("cv_pcr.png")
validationplot(pcr.fit,val.type="MSEP")
dev.off()