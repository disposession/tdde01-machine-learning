# Implement a ridge regression applied to the data 'swiss'
df = swiss
ridgeRegression <- function(X, Y, lambda){
  DesignMatrix <- scale(X)
  ResponseVector <- scale(Y)

  p = dim(DesignMatrix)[2]

  # Calculate
  # Size of slopeVector equals dim(DesignMatrix)[2]+1
  temp = t(DesignMatrix) %*% as.matrix(DesignMatrix)

  # This is where the lambda penalty comes in
  # diag(p) is the identity matrix of size p
  temp = temp + lambda * diag(p)
  
  slopeVector <<- solve(temp) %*% t(DesignMatrix) %*% ResponseVector

  #fittedValuesVector <<- as.matrix(DesignMatrix) %*% slopeVector
  #residualsVector <<- ResponseVector - fittedValuesVector

  return(slopeVector)
}

# Fertility is target and the remaining variables are features. 
lam = seq(0, 2000, 0.1)
data = df[,-1]
target = df$Fertility
coefs = matrix(ncol = dim(data)[2])
for (lambda in lam) {
  coef = ridgeRegression(data, target, lambda)
  coefs = rbind(coefs, t(coef))
}
coefs = coefs[-1,]

# Make a plot that shows how coefficients depend on various lambda values. 
plot(c(-2,range(log(lam))[2]), range(coefs), type="n", xlab = "Log Lambda", ylab="Coefficients")
colors = c("black", "red", "green", "blue", "cyan")
for(i in 1:ncol(coefs)){
  lines(log(lam),coefs[,i], col=colors[i])
  # print(which.max(fit$coef[i,]))
}


# Implement same regression with glmnet package
fit1 = glmnet(scale(as.matrix(df[,-1])), scale(df$Fertility), alpha=0, family="gaussian")
# Make a plot that shows how coefficients depend on various lambda values. 
plot(fit1, xvar="lambda", label=TRUE, ylim=c(-0.6, 0.3))

# compare the plots and make conclusions.

